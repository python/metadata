import time
import os
import humanize

file=input("File : ")
metadata=os.stat(file)

def dformat(date) :
    if date <= 9 : return "0"+str(date)
    else : return str(date)

def ddformat(metadate) :
    return dformat(metadate.tm_mday)+'/'+dformat(metadate.tm_mon)+'/'+str(metadate.tm_year)


print("\ntaille :",humanize.naturalsize(metadata.st_size,True),"("+humanize.naturalsize(metadata.st_size)+")")

print("création :",ddformat(time.localtime(metadata.st_ctime)))

print("dernière modification :",ddformat(time.localtime(metadata.st_mtime)))

print("dernier accès :",ddformat(time.localtime(metadata.st_atime)))
